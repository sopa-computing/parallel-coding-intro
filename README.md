# Introduction to Parallel Coding

This Git repository contains example codes used for the 3-part
**Introduction to Parallel Coding** Research Computing Lunchtime Taster
session given in March/April 2019.

Further details and slides for these sessions can be found at:

https://www.wiki.ed.ac.uk/display/PandAIntranet/Introduction+to+Parallel+Coding+-+Lunchtime+Taster

## Part 1 - Introduction

C and Fortran versions of the serial codes for this part can be found
in the **serial-c** and **serial-fortran** folders respectively.

See the **README.md** file in each folder for instructions on
compiling and running the codes.

## Part 2 - MPI

MPI versions of the C and Fortran codes introduced in Part 1 can be found in the
**mpi-c** and **mpi-fortran** folders, as well as a few additional example
codes demonstrated during the MPI session.

See the **README.md** file in each folder for instructions on
compiling and running the codes.

## Part 3 - OpenMP

OpenMP versions of the C and Fortran codes introduced in Part 1 can be found in the
**openmp-c** and **openmp-fortran** folders.

See the **README.md** file in each folder for instructions on
compiling and running the codes.
