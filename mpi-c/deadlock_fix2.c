#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/**
 * Another solution for resolving the deadlock in deadlock.c
 *
 * Here we replace the blocking sends with non-blocking sends.
 * This allows all processes to proceed to the receive, where they'll
 * block until receiving the data previously sent by their neighbour.
 */
int main(void) {
    const MPI_Comm mpi_comm = MPI_COMM_WORLD; // We'll always use the default MPI communicator here
    const int mpi_tag = 0; // We will use a single tag for all MPI communication here
    int mpi_size; // MPI world size (= total number of parallel processes)
    int mpi_rank; // MPI world rank (identifies this process)
    int prev_rank, next_rank; // Previous & next MPI ranks
    int message_to_send;
    int message_received;
    MPI_Request mpi_request;

    // Initialize the MPI environment.
    MPI_Init(NULL, NULL);

    // Get details about the total number of parallel processes
    // being run (mpi_size) and this particular process (mpi_rank).
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    // Work out previous & next MPI ranks, treating the processes as a circle
    next_rank = mpi_rank + 1;
    if (next_rank == mpi_size) next_rank = 0;
    prev_rank = mpi_rank - 1;
    if (prev_rank == -1) prev_rank = mpi_size - 1;

    // Create (rubbishy) message to be sent by this rank
    message_to_send = 100 + mpi_rank;

    //============ MPI communication fun starts here ==============

    printf("Rank %d is initiating a non-blocking send of message '%d' to rank %d\n", mpi_rank, message_to_send, next_rank);
    MPI_Issend(&message_to_send, 1, MPI_INTEGER, next_rank, mpi_tag, mpi_comm, &mpi_request);

    printf("Rank %d is initiating a blocking receive of a message from rank %d\n", mpi_rank, next_rank);
    MPI_Recv(&message_received, 1, MPI_INTEGER, prev_rank, mpi_tag, mpi_comm, MPI_STATUS_IGNORE);
    printf("Rank %d received message '%d' from rank %d\n", mpi_rank, message_received, prev_rank);

    printf("Rank %d is waiting for the previous non-blocking send to complete\n", mpi_rank);
    MPI_Wait(&mpi_request, MPI_STATUS_IGNORE);
    printf("Rank %d has completed non-blocking message send\n", mpi_rank);

    //============ MPI communication fun ends here ==============

    // Shut down the MPI environment.
    MPI_Finalize();

    // Exit as normal
    return EXIT_SUCCESS;
}
