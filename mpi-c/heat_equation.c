#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

/**
 * Numerical approximation of the heat equation, using
 * discrete finite difference methods.
 * MPI implementation.
 *
 * Please see the C version for background information
 * and an explanation of the key variables.
 *
 *
 * How we are parallelising this problem
 * =====================================
 *
 * Let np = number of parallel tasks == MPI "world size"
 *
 * The approach used here is to divide the x-axis into segments
 * of (almost) equal size. [We say almost as Nx may not divide
 * exactly by np, so we add an extra point to some partitions
 * to account for the remainder.]
 *
 * Each process then works solely on its partition.
 *
 * However, recall that our difference equation calculates the
 * next time step value T(i,j+1) via:
 *
 * T(i,j+1) = T(i,j) + r [T(i-1,j) - 2T(i,j) + T(i+1,j)]
 *
 * This means the T(i,j+1) value needs the value for T(i,j)
 * *AND* its left and right neighbours. This is fine when we're
 * strictly inside our partition, but more complex at the endpoints.
 *
 * So we need to know the current value of the points just outside
 * our partition, and these are calculated by neighbouring MPI ranks.
 * Thus we need to pass the current values for our endpoints to our
 * neighbours at the start of each time step.
 *
 * Here's a concrete example: Nx = 13, np = 3
 *
 *                                  *
 *                              *   *   *
 *
 *          +---+---+---+---+---+---+---+---+---+---+---+---+---+
 * i:       0   1   2   3   4   5   6   7   8   9  10  11  12  13(=Nx)
 * Rank 0: (0)  1   2   3   4  (5)
 * Rank 1:                 (4)  5   6   7   8  (9)
 * Rank 2:                                 (8)  9  10  11  12 (13)
 *
 * Rank 0 calculates the next values for i=1,2,3 and 4. It needs
 * the current values for i=0 & i=5 for this. i=0 is set by the left
 * boundary condition. i=5 is computed by rank 1, so its current value
 * needs to get passed to this rank at the start of each time step.
 *
 * Rank 1 calculates the next values for i=5,6,7 and 8. It needs the
 * current values for i=4 and i=9 for this. i=4 is computed by rank 0
 * and i=9 is computed by rank 2, so the current values for these need to
 * be passed to this rank by ranks 0 & 2 at the start of each time step.
 *
 * [... For np > 3 there there would be more cases like rank 1 ...]
 *
 * Rank 2 calculates the next values for i=9,10,11 and 12. It needs the
 * current values for i=8 and i=13 for this. i=13 is set by the right
 * boundary condition. i=8 is computed by rank 1, so its current value
 * needs to get passed to this rank at the start of each time step.
 *
 * For each rank (k), we'll use the following variables:
 *
 * i_start = leftmost i index (in brackets above), i.e. the index just
 *           before the ones we're going to be updating
 * i_end   = rightmost i index (in brackets above), i.e. the index just
 *           after the ones we're going to be updating
 * i_width = i_end - i_start + 1 = total number of points in this partition,
 *           including the bracketed boundary points.
 *
 * We store values in an array local to this rank/process. In the code
 * you'll notice that we need to convert 'i' values to array indices,
 * which we are representing by the variable 'ki' in the code below.
 * E.g. for rank 1: i_start = 4, which should map to index ki=0.
 *
 */

// User-specified parameters
static long Nx; // Number of x sub-intervals to pick from interval [0,1]
static double t_end; // Final time to reach

// The following parameters are currently hard-coded
static const double alpha = 0.001; // alpha coefficient in heat equation
static const double r = 0.25; // Must be <1/2 for convergence

// Declare functions defined further down
static double initial_condition(double x);
static int read_parameters(int argc, char *argv[]);
static void print_values(double *values);
static void debug_values(double *values, long count);

// Helper macro to compute the minimum of 2 numbers, used to help divvy
// up the parallel workload.
// [You would think the C standard library would provide a min() function,
// wouldn't you...?!]
#define MIN(a,b) ((a) < (b) ? (a) : (b))

int main(int argc, char *argv[]) {
    int mpi_size, mpi_rank; // MPI world size & rank
    MPI_Request mpi_request; // Used as a handle for a non-blocking MPI send
    MPI_Status mpi_status; // Used to record status of an MPI call
    const MPI_Comm mpi_comm = MPI_COMM_WORLD; // We'll always use the default MPI_COMM_WORLD communicator here
    const int mpi_tag = 0; // We can get away with using a single tag for all MPI communication here
    double dt; // Time increment (computed from params)
    long Nt; // Number of time iterations required (computed from params)
    long i_start, i_end, i_width; // See above
    long quotient, remainder; // Used to calculate partition size
    double *curr_values, *next_values; // Memory arrays for storing current & next time step values
    long i, j; // i = discrete 'x' point (0..Nx), j = discrete 't' point (0..Nt)
    int k; // MPI rank (used when aggregating results)
    long ki; // Rank-specific array index within curr_values (== i - i_start)
    double *temp; // Used for swapping buffers
    double diff; // Temporary variable used during inner calculation loop

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);
    MPI_Comm_size(mpi_comm, &mpi_size);
    MPI_Comm_rank(mpi_comm, &mpi_rank);

    // Read in user-specified parameters
    if (!read_parameters(argc, argv)) {
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Make sure Nx is big enough to be partitioned in a non-degenerate way
    // [Also note that the current initial condition is a bit degenerate for
    // small Nx anyway!]
    if (Nx < mpi_size + 1) {
        fprintf(stderr, "This parallel algorithm requires Nx >= np + 1.\n");
        fprintf(stderr, "Please increase Nx (currently %ld) or decrease np (currently %d).\n", Nx, mpi_size);
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Calculate computed parameters.
    // We first pick dt so that the resulting r will ensure convergence,
    // then calculate how many time steps we need (Nt) to reach required end time.
    dt = r / (Nx * Nx * alpha);
    Nt = (long) ceil(t_end / dt);

    if (mpi_rank == 0) {
        printf("Input parameters: Nx=%ld t_end=%f alpha=%e r=%f\n", Nx, t_end, alpha, r);
        printf("Computed parameters: dt=%e Nt=%ld\n", dt, Nt);
    }

    // Work out which x-partition this rank will be calculating
    quotient = (Nx - 1) / mpi_size;
    remainder = (Nx - 1) % mpi_size;
    i_start = mpi_rank * quotient + MIN(mpi_rank, remainder);
    i_end = (mpi_rank + 1) * quotient + 1 + MIN(mpi_rank + 1, remainder);
    i_width = i_end - i_start + 1; // NB: This includes left & right overlaps

    // Print out partition info for debugging purposes
    printf("Rank: %d i_start=%ld i_end=%ld i_width=%ld (q=%ld r=%ld)\n",
            mpi_rank, i_start, i_end, i_width, quotient, remainder);

    // Allocate memory arrays to hold current & next data for this rank
    curr_values = malloc(sizeof(double) * i_width);
    next_values = malloc(sizeof(double) * i_width);

    // Set up initial conditions
    // We will first use curr_values to store the current (initial, j=0) time step data,
    // and next_values for the next (j=1) data.
    for (ki = 0; ki < i_width; ++ki) {
        i = i_start + ki;
        curr_values[ki] = initial_condition((double) i / Nx);
    }

//    printf("INITIAL values for rank %d:\n", mpi_rank);
//    debug_values(curr_values, i_width);

    // Now we do the main calculation. This is a nested loop,
    // iterating over time (j) first, then over x values (i).
    for (j = 1; j <= Nt; ++j) {
        // Compute and fill in next array of values for this time step.
        // The new values are saved into the next_values array.
        for (ki = 1; ki < i_width - 1; ++ki) {
            diff = curr_values[ki+1]- 2 * curr_values[ki] + curr_values[ki-1];
            next_values[ki] = curr_values[ki] + r * diff;
        }

        // Preserve boundary conditions (on first & last processes)
        if (mpi_rank == 0) {
            next_values[0] = curr_values[0];
        }
        if (mpi_rank == mpi_size - 1) {
            next_values[i_width - 1] = curr_values[i_width - 1];
        }

        // We now perform a "halo exchange" to send our new endpoint values
        // to adjacent MPI ranks.
        //
        // Note in the following we're using non-blocking MPI_Issend() calls
        // to avoid potential for deadlocks.
        //
        // TODO: Though I suspect the overall asymmetry here would prevent a deadlock
        // arising if we had used plain old MPI_Ssend() calls, but I need to check this
        // properly...
        //
        // Part 1: Pass left endpoint results leftwards
        if (mpi_rank > 0) {
            MPI_Issend(next_values + 1, 1, MPI_DOUBLE, mpi_rank-1, mpi_tag, mpi_comm, &mpi_request);
            MPI_Wait(&mpi_request, &mpi_status);
        }
        if (mpi_rank < mpi_size - 1) {
            MPI_Recv(next_values + i_width - 1, 1, MPI_DOUBLE, mpi_rank+1, mpi_tag, mpi_comm, &mpi_status);
        }
        // Part 2: Pass right endpoint results rightwards
        if (mpi_rank < mpi_size - 1) {
            MPI_Issend(next_values + i_width - 2, 1, MPI_DOUBLE, mpi_rank+1, mpi_tag, mpi_comm, &mpi_request);
            MPI_Wait(&mpi_request, &mpi_status);
        }
        if (mpi_rank > 0) {
            MPI_Recv(next_values, 1, MPI_DOUBLE, mpi_rank-1, mpi_tag, mpi_comm, &mpi_status);
        }

        // Next we swap curr_values & next_values for the next iteration
        temp = curr_values;
        curr_values = next_values;
        next_values = temp;
    }

    printf("FINAL values for rank %d:\n", mpi_rank);
    debug_values(curr_values, i_width);

    if (mpi_rank) {
        // Worker process. Send results to manager
        printf("Rank %d sending final results to manager\n", mpi_rank);
        MPI_Ssend(curr_values, i_width, MPI_DOUBLE, 0, mpi_tag, mpi_comm);
    }
    else {
        // Manager process. Build (reduce) final result array.
        //
        // Exercise for reader: rewrite the code below
        // to print out the result without first building a
        // final result array :-)
        //
        double *result = malloc(sizeof(double) * (Nx + 1));

        // First fill in own partial results, which will fill the
        // first segment of the resulting result array.
        // [Making the manager == rank 0 helps here!]
        i = 0;
        for (ki = 0; ki < i_width; ++ki) {
            result[i] = curr_values[ki];
            ++i;
        }
        i -= 2; // to account for halo - next rank overlaps by 2

        // Then merge in results from other ranks. We'll do these
        // in exact order, as that's a bit easier.
        for (k = 1; k < mpi_size; ++k) {
            int recv_count;
            // Receive partial result from rank k.
            // Rather than work out the size of the expected result -
            // which requires some of the gnarly integer arithmetic above -
            // let's just pass it the whole of the remaining result buffer,
            // then extract the number of items actually passed.
            printf("Manager receiving final results from rank %d\n", k);
            MPI_Recv(result + i, Nx + 1 - i, MPI_DOUBLE, k, mpi_tag, mpi_comm, &mpi_status);
            MPI_Get_count(&mpi_status, MPI_DOUBLE, &recv_count);
            i += recv_count - 2; // -2 is to account for halo, as above
        }

        // Output final results
        puts("T(.,t) values at final time step:");
        print_values(result);
        free(result);
    }

    // Clean up and exit
    free(curr_values);
    free(next_values);
    MPI_Finalize();
    return EXIT_SUCCESS;
}

/* This function defines our initial condition.
 * We'll use a simple step function for this.
 */
double initial_condition(double x) {
    return (x >= 0.4 && x <= 0.6) ? 1 : 0.2;
}

/* Reads in the user-specified command line arguments
 * and sets the appropriate globals as appropriate.
 *
 * Returns non-zero if all arguments have been specified
 * correctly, otherwise zero.
 */
int read_parameters(int argc, char *argv[]) {
    if (argc != 3) {
        fputs("Required arguments: <x_partitions(Nx)> <t_end>\n", stderr);
        return 0;
    }
    Nx = atol(argv[1]);
    if (Nx < 1) {
        fputs("Number of x partitions must be >= 1\n", stderr);
        return 0;
    }
    t_end = atof(argv[2]);
    if (t_end <= 0.0) {
        fputs("Time endpoint must be positive\n", stderr);
        return 0;
    }
    return 1;
}

/* Prints the given array of values for a particular time step */
void print_values(double *values) {
    long i;

    printf("[");
    for (i = 0; i <= Nx; ++i) {
        printf("%.10f", values[i]);
        if (i < Nx) printf(", ");
    }
    printf("]\n");
}

/* Helper for debugging rank-specific values */
void debug_values(double *values, long count) {
    long i;

    printf("[");
    for (i = 0; i < count; ++i) {
        if (i == 0 || i == count - 1) printf("(");
        printf("%.10f", values[i]);
        if (i == 0 || i == count - 1) printf(")");
        if (i < count - 1) printf(", ");
    }
    printf("]\n");
}
