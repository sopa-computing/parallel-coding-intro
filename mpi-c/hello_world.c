#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/**
 * Hello World, C style!
 *
 * MPI version. This simply gets each parallel MPI task (rank) to
 * independently say Hello World, and report its rank number.
 */
int main(void) {
    int mpi_size; // MPI world size (= total number of parallel processes)
    int mpi_rank; // MPI world rank (identifies this process)

    // Initialize the MPI environment.
    // This should be first thing you do in any MPI program.
    //
    // Note: The NULL arguments here are because this code
    // doesn't bother with command line parameters.
    // Other examples will demonstrate what happens when we do
    // have parameters.
    MPI_Init(NULL, NULL);

    // Get details about the total number of parallel processes
    // being run (mpi_size) and this particular process (mpi_rank).
    //
    // Note: You might first expect these functions to *return*
    // the data you want. Instead you have to pass a pointer to
    // a variable that will hold your result, and the function will
    // update the variable's data. MPI functions generally return
    // an error code, which we're ignoring in this example.
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    // Embellish our original "Hello World" message to
    // report this process' rank & size
    printf("Hello World! (This is parallel rank %d out of %d)\n",
            mpi_rank, mpi_size);

    // Shut down the MPI environment.
    // This must be done just before exiting your MPI program.
    // [Remember also to do this if you need to exit early.]
    MPI_Finalize();

    // Exit as normal
    return EXIT_SUCCESS;
}
