#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

/**
 * Estimation of Pi via Monte Carlo simulation.
 *
 * MPI C version. Make sure you've first looked at the
 * serial version of this code *and* the MPI version of
 * hello_world.c.
 *
 *
 * How we are parallelising this problem
 * =====================================
 *
 * We simply get each individual MPI process to do
 * a (roughly) equal share of the total number of requested
 * trials and record the number of successful Monte Carlo
 * trials (success_count).
 *
 * Once each process has finished its work, it sends its
 * success_count to process with rank 0 which, as is common,
 * acts as the "manager" process to coordinate all of the work.
 *
 * The manager process receives all of the success_counts
 * from the other "worker" processes and adds them up to
 * calculate the total number of successes recorded over all
 * processes.
 *
 * It can then trivially compute the estimate of Pi in exactly
 * the same way as the serial code.
 */

// Declare functions defined further below
static long do_monte_carlo(long num_trials);
static double next_random();

int main(int argc, char *argv[]) {
    int mpi_size, mpi_rank; // MPI world size & rank
    const MPI_Comm mpi_comm = MPI_COMM_WORLD; // We'll always use the default MPI communicator here
    const int mpi_tag = 0; // We can get away with using a single tag for all MPI communication here
    long num_trials_total; // Total number of Monte Carlo trials to perform
    long num_trials_this_process; // Number of MC trials to be done by this process
    long remainder; // Remainder when dividing num_trials_total by MPI world size
    long success_count; // Number of MC successes recorded by this process
    double pi_estimate; // Final Pi estimate (computed by manager process only)

    // Initialize the MPI environment
    // Note how we pass (pointers to) argc & argv to MPI_Init() here.
    MPI_Init(&argc, &argv);
    MPI_Comm_size(mpi_comm, &mpi_size);
    MPI_Comm_rank(mpi_comm, &mpi_rank);

    // Read requested number of trials from command line arguments
    if (argc != 2) {
        fputs("Required argument: <number of trials>\n", stderr);
        MPI_Finalize();
        return EXIT_FAILURE;
    }
    num_trials_total = atol(argv[1]);
    if (num_trials_total <= 0) {
        fputs("Number of trials must be a positive long integer\n", stderr);
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Work out the number of trials to do per process.
    // This is computed with a bit of integer division, which
    // of course rounds downward, so we perform 1 extra trial on
    // some of the ranks to make up the remainder.
    num_trials_this_process = num_trials_total / mpi_size;
    remainder = num_trials_total % mpi_size;
    if (mpi_rank < remainder) {
        ++num_trials_this_process;
    }

    // "Seed" the random number generator so that each rank
    // generates a different sequence of random numbers
    srand(time(NULL) + mpi_rank);

    // Run specified number of Monte Carlo trials
    success_count = do_monte_carlo(num_trials_this_process);

    // Report the partial result for this rank, which is handy for
    // understanding what's going on.
    printf("Partial result for rank=%d: num_trials=%ld success_count=%ld\n",
            mpi_rank, num_trials_this_process, success_count);

    if (mpi_rank != 0) {
        // This is a worker process.
        // We send our partial result to the manager process (0).
        //
        // We use MPI_Ssend() for this, which is the simplest way
        // of sending an MPI message.
        // It sends the message immediately (=synchronous),
        // then waits until the other process receives the
        // message (=blocking).
        //
        // This simple method works fine in this example, but you need
        // to be careful in more complex cases as you can end up with a
        // deadlock.
        MPI_Ssend(&success_count, 1, MPI_LONG, 0, mpi_tag, mpi_comm);
    }
    else {
        // This is the manager process.
        // We will receive results from each of the worker processes
        // and tot up our final result (success_count_total), which
        // we'll start off as the number of successes recorded by this
        // process.
        long success_count_total = success_count;
        long received_success_count; // Used to receive data from worker processes
        for (int i = 1; i < mpi_size; i++) {
            // Receive next incoming worker process result
            // into received_success_count variable.
            // We use MPI_ANY_SOURCE here to indicate we're happy to
            // receive results from any rank in whichever order to come in -
            // everything should tally up in the end.
            MPI_Recv(&received_success_count, 1, MPI_LONG, MPI_ANY_SOURCE,
                    mpi_tag, mpi_comm, MPI_STATUS_IGNORE);
            // Add the received count to the final total
            success_count_total += received_success_count;
        }
        // Compute our resulting estimate for pi
        pi_estimate = 4.0 * success_count_total / num_trials_total;

        // Output final result
        printf("Final result: pi=~%0.8f num_trials=%ld success_count=%ld\n",
                pi_estimate, num_trials_total, success_count_total);
    }

    // Clean up and exit happily
    MPI_Finalize();
    return EXIT_SUCCESS;
}

/**
 * We define a wee helper macro to square a number. This is used
 * in do_monte_carlo() below to make things a bit easier to read.
 */
#define SQUARE(x) ((x)*(x))

/**
 * Runs the Monte Carlo simulation for the given number
 * of trials, returning the number of successful samples.
 */
long do_monte_carlo(long num_trials) {
    long i, success_count;
    double x, y;

    success_count = 0;
    for (i = num_trials; i > 0; --i) {
        x = next_random();
        y = next_random();
        if (SQUARE(x-0.5) + SQUARE(y-0.5) < SQUARE(0.5)) {
            ++success_count;
        }
    }
    return success_count;
}

/**
 * Returns a pseudo-random number in the range [0,1).
 *
 * NOTE: You would probably want to use a better random
 * number generator than this!
 */
double next_random() {
    return (double) rand() / RAND_MAX;
}
