#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/* Example of simple point 2 point communication.
 *
 * This code is intended to run with 2 MPI processes.
 * Rank 0 sends a message to rank 1, and rank 1 receives this message.
 */
int main(void) {
    const MPI_Comm mpi_comm = MPI_COMM_WORLD; // We'll always use the default MPI communicator here
    const int mpi_tag = 0; // We will use a single tag for all MPI communication here
    int mpi_size; // MPI world size (= total number of parallel processes)
    int mpi_rank; // MPI world rank (identifies this process)

    // Initialize the MPI environment.
    MPI_Init(NULL, NULL);

    // Get details about the total number of parallel processes
    // being run (mpi_size) and this particular process (mpi_rank).
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    // Sanity check MPI size - this example needs 2 parallel processes
    if (mpi_size != 2) {
        fputs("This example is intended to run with 2 parallel processes.\nPlease relaunch with -np 2.\n", stderr);
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    //============ MPI communication fun starts here ==============

    if (mpi_rank == 0) {
        // We will send a simple message (the number 42) to rank 1.
        // We use the simplest MPI_Ssend() function for this, which blocks
        // until the message gets received
        int to_send = 42;
        printf("Rank 0 is calling MPI_SSend() to send a message %d to rank 1\n", to_send);
        MPI_Ssend(&to_send, 1, MPI_INTEGER, 1, mpi_tag, mpi_comm);
        printf("Rank 0 has successfully completed the blocking MPI_Ssend() call\n");
    }
    else if (mpi_rank == 1) {
        // We will receive the message that rank 0 is sending us.
        // We use the simplest MPI_Recv() function for this, which blocks
        // until the message gets received
        int received = 0;
        printf("Rank 1 is calling MPI_Recv() to receive a message from rank 0\n");
        MPI_Recv(&received, 1, MPI_INTEGER, 0, mpi_tag, mpi_comm, MPI_STATUS_IGNORE);
        printf("Rank 1 has successfully completed the blocking MPI_Recv() call and received message %d\n", received);
    }

    //============ MPI communication fun ends here ==============

    // Shut down the MPI environment.
    MPI_Finalize();

    // Exit as normal
    return EXIT_SUCCESS;
}
