# IMPORTANT!
#
# This Makefile should work without modification on the
# School Linux platform and on Eddie. However you must
# first add OpenMPI (or an alternative MPI implementation)
# to your environment by executing one of the following:
#
# module load openmpi
# OR
# module load mpich [NB: Not available on Eddie]
# OR
# module load intel
#
# These will all use the default (GCC) compiler suite.
#
# If you prefer to use Intel MPI with the Intel compiler,
# you should do:
#
# module load intel
#
# and must then also change compile_options below to
# invoke the Intel compiler.
#
######################################################

# GCC compiler options (default)
compiler = mpif90
compile_options = -std=f95 -fall-intrinsics -Wall -Wpedantic -O2
linker_options = -lm

# Intel compiler options
# Uncomment this if you want to use the Intel compiler
# (which necessarily means using Intel's MPI as well)
#compiler = mpiifort
#compile_options = -O2
#linker_options =

######################################################

sources    = $(wildcard *.f90)
binaries   = $(patsubst %.f90, %, $(sources))

######################################################

all: $(binaries)

.PHONY: clean

clean:
	rm -f $(binaries) $(assemblies)

$(binaries): %: %.f90
	$(compiler) $(compile_options) -o $@ $< $(linker_options)
