# MPI Fortran codes

This directory contains the MPI versions of the example serial codes,
plus some other example codes, all written in Fortran (95ish).

**Disclaimer:** I'm not very familiar with Fortran. This code works but
may not follow best practice! A Fortran guru would be most welcome to come
and improve this code!

If you prefer C (as I do!), then C versions of this code can be found in the
adjacent **mpi-c** folder.

## Details about these codes

* **hello\_world.f90**: MPI version of serial Hello World example code
* **monte\_carlo_pi.f90**: MPI version of serial Monte Carlo Pi example code
* **heat\_equation.f90**: MPI version of serial Heat Equation example code
* **simple\_p2p.f90**: Demonstration of simple 2-process point-to-point MPI communication
* **deadlock.f90**: Demonstration of how a deadlock can arise in MPI
* **deadlock\_fix.f90**: Resolution of deadlock in **deadlock.f90** by pairing up sends & receives
* **deadlock\_fix2.f90**: Resolution of deadlock in **deadlock.f90** by using non-blocking communications

## How to compile the codes

The codes are written in C so first need to be **compiled** into runnable
binary codes using the compiler wrapper provided by your MPI implementation.

I have provided a **Makefile** to do this for you. (A Makefile is
essentially an old-fashioned and hard-to-read recipe for building code projects.)
You can invoke this on any School Linux computer or on Eddie by typing:

```sh
module load openmpi
make
```

Note that you need to run this from the directory containing this file.

This Makefile should work on any Linux computer with the GCC compiler suite,
and it includes some alternative instructions for compiling and/or running
the code using the Intel compiler suite,
which is available on School Linux computers and on Eddie.
If you're familiar with building Fortran code, it should be possible to tweak the
Makefile to make the code work with other compilers or on MacOS or Windows
computers.

## How to run the codes

Once compiled, you can run the codes using 4 parallel MPI processes
by typing in:

```sh
mpirun -np 4 ./hello_world

OR

mpirun -np 4 ./monte_carlo_pi 1000000

OR

mpirun -np 4 ./heat_equation 100 10

etc...
```

**Note:** If running on a School Linux computer or on Eddie then you
will need to have first done the above `module load ...` command first.

Note also that the **monte\_carlo\_pi** and **heat\_equation** codes require you to
supply one or two parameters. You'll get told what information you need
if you simply try to run them without parameters.
