! One solution for resolving the deadlock in deadlock.f90
!
! Here we keep using blocking sends & receives, but switch
! the order in which they happen so that they pair up correctly.
program deadlock_fix1
    use mpi
    implicit none
    integer, parameter :: tag = 0 ! We can get away with using a single tag for all MPI communication here
    integer :: mpi_size, mpi_rank ! MPI world size & rank
    integer :: prev_rank, next_rank; ! Previous & next MPI ranks
    integer :: message_to_send
    integer :: message_received;
    integer :: ierr ! Used to check for errors in MPI calls

    ! Initialize the MPI environment.
    ! This should be first thing you do in any MPI program.
    call MPI_Init(ierr)

    ! Get details about this process (mpi_rank)
    ! and the total number of processes (mpi_size)
    call MPI_Comm_size(MPI_COMM_WORLD, mpi_size, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, mpi_rank, ierr)

    ! Work out previous & next MPI ranks, treating the processes as a circle
    next_rank = mpi_rank + 1;
    if (next_rank == mpi_size) then
        next_rank = 0
    endif
    prev_rank = mpi_rank - 1;
    if (prev_rank == -1) then
        prev_rank = mpi_size - 1
    endif

    ! Create (rubbishy) message to be sent by this rank
    message_to_send = 100 + mpi_rank;

    ! ============ MPI communication fun starts here ==============

    if (mod(mpi_rank, 2) == 0) then
        ! Even ranks will do the blocking send (to odd ranks) first, then a receive
        write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, &
                " is initiating a blocking send of message '", message_to_send, &
                " ' to rank ", next_rank
        call MPI_Ssend(message_to_send, 1, MPI_INTEGER, next_rank, tag, MPI_COMM_WORLD, ierr);

        write(*,"(A,I0,A,I0)") "Rank ", mpi_rank, &
                "  is initiating a blocking receive of a message from rank ", prev_rank
        call MPI_Recv(message_received, 1, MPI_INTEGER, prev_rank, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr);
        write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, " received message '", message_received, &
                "' from rank ",  prev_rank
    else
        ! Odd ranks will do the blocking receive (from even ranks) first, then a send
        write(*,"(A,I0,A,I0)") "Rank ", mpi_rank, &
                "  is initiating a blocking receive of a message from rank ", prev_rank
        call MPI_Recv(message_received, 1, MPI_INTEGER, prev_rank, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr);
        write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, " received message '", message_received, &
                "' from rank ",  prev_rank

        write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, &
                " is initiating a blocking send of message '", message_to_send, &
                " ' to rank ", next_rank
        call MPI_Ssend(message_to_send, 1, MPI_INTEGER, next_rank, tag, MPI_COMM_WORLD, ierr);
    end if

    ! ============ MPI communication fun ends here ==============

    ! Shut down the MPI environment.
    call MPI_Finalize(ierr)

end program deadlock_fix1
