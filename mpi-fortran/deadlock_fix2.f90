! Another solution for resolving the deadlock in deadlock.c
!
! Here we replace the blocking sends with non-blocking sends.
! This allows all processes to proceed to the receive, where they'll
! block until receiving the data previously sent by their neighbour.
program deadlock_fix2
    use mpi
    implicit none
    integer, parameter :: tag = 0 ! We can get away with using a single tag for all MPI communication here
    integer :: mpi_size, mpi_rank ! MPI world size & rank
    integer :: prev_rank, next_rank; ! Previous & next MPI ranks
    integer :: message_to_send
    integer :: message_received;
    integer :: ierr ! Used to check for errors in MPI calls
    integer :: my_mpi_request ! Used as a handle for a non-blocking MPI send

    ! Initialize the MPI environment.
    ! This should be first thing you do in any MPI program.
    call MPI_Init(ierr)

    ! Get details about this process (mpi_rank)
    ! and the total number of processes (mpi_size)
    call MPI_Comm_size(MPI_COMM_WORLD, mpi_size, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, mpi_rank, ierr)

    ! Work out previous & next MPI ranks, treating the processes as a circle
    next_rank = mpi_rank + 1;
    if (next_rank == mpi_size) then
        next_rank = 0
    endif
    prev_rank = mpi_rank - 1;
    if (prev_rank == -1) then
        prev_rank = mpi_size - 1
    endif

    ! Create (rubbishy) message to be sent by this rank
    message_to_send = 100 + mpi_rank;

    ! ============ MPI communication fun starts here ==============

    write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, &
            " is initiating a non-blocking send of message '", message_to_send, &
            " ' to rank ", next_rank
    call MPI_Issend(message_to_send, 1, MPI_INTEGER, next_rank, tag, MPI_COMM_WORLD, my_mpi_request, ierr);

    write(*,"(A,I0,A,I0)") "Rank ", mpi_rank, &
            "  is initiating a blocking receive of a message from rank ", prev_rank
    call MPI_Recv(message_received, 1, MPI_INTEGER, prev_rank, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr);
    write(*,"(A,I0,A,I0,A,I0)") "Rank ", mpi_rank, " received message '", message_received, &
            "' from rank ",  prev_rank

    write(*,"(A,I0,A)") "Rank ", mpi_rank, &
            " is waiting for the previous non-blocking send to complete"
    call MPI_Wait(my_mpi_request, MPI_STATUS_IGNORE, ierr)
    write(*,"(A,I0,A)") "Rank ", mpi_rank, " has completed non-blocking message send"

    ! ============ MPI communication fun ends here ==============

    ! Shut down the MPI environment.
    call MPI_Finalize(ierr)

end program deadlock_fix2
