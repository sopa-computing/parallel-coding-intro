! Heat equation, MPI Fortran version.
!
! Please see the C versions for background information
! and an explanation of the variables.
program heat_equation
    use mpi
    implicit none
    integer :: mpi_size, mpi_rank ! MPI world size & rank
    integer, parameter :: tag = 0 ! We can get away with using a single tag for all MPI communication here
    integer, dimension(MPI_STATUS_SIZE) :: my_mpi_status ! Used to record status of an MPI call
    integer :: my_mpi_request ! Used as a handle for a non-blocking MPI send
    integer :: ierr ! Used to check for errors in MPI calls
    logical :: valid ! Tests whether user-specified parameters are all valid
    integer :: Nx ! Number of x sub-intervals to pick from interval [0,1]
    double precision :: t_end ! Final time to reach
    double precision, parameter :: alpha = 0.001d0 ! alpha coefficient in heat equation
    double precision, parameter :: r = 0.25d0 ! Must be <1/2 for convergence
    double precision :: dt ! Time increment (computed from params)
    integer :: Nt ! Number of time iterations required (computed from params)
    integer :: i, j ! i = discrete 'x' point (0..Nx), j = discrete 't' point (0..Nt)
    integer :: i_start, i_end, i_width ! Partition being handled by this process. See C version for details
    integer :: quotient, remainder ! Used to calculate partition size
    integer :: k ! MPI rank (used when aggregating results)
    integer :: ki ! Rank-specific array index within curr_values (== i - i_start)
    integer :: recv_count ! Used to help build up final result
    double precision, dimension(:), allocatable, target :: work1, work2 ! Memory arrays used for curr_values & next_values (below)
    double precision, dimension(:), pointer :: curr_values, next_values ! Pointers to memory arrays for storing current & next time step values
    double precision, dimension(:), pointer :: temp ! Used for swapping buffers
    double precision, dimension(:), allocatable, target :: result ! Array for bulding final result (used by manager process only)
    double precision, dimension(:), pointer :: resultptr
    double precision :: diff ! Temporary variable used during inner calculation loop
    double precision :: initial_condition ! Function to compute initial conditions (see below)
    interface
        subroutine print_values(values)
            double precision, dimension(:), pointer :: values
        end subroutine
        subroutine debug_values(heading, mpi_rank, values, count)
            character(len=*) :: heading
            integer :: mpi_rank
            double precision, dimension(:), pointer :: values
            integer :: count
        end subroutine
    end interface

    ! Initialize the MPI environment.
    call MPI_Init(ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, mpi_size, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, mpi_rank, ierr)

    ! Read in and validate user-specified parameters
    call read_parameters(Nx, t_end, valid)
    if (.not. valid) then
        call MPI_Finalize(ierr)
        stop 1
    end if

    ! Make sure Nx is big enough to be partitioned in a non-degenerate way
    ! [Also note that the current initial condition is a bit degenerate for
    ! small Nx anyway!]
    if (Nx < mpi_size + 1) then
        write(0,"(A)") "This parallel algorithm requires Nx >= np + 1."
        write(0,"(A,I0,A,I0,A)") "Please increase Nx (currently ", Nx, &
            ") or decrease np (currently ", mpi_size, ")"
        call MPI_Finalize(ierr)
        stop 1
    end if

    ! Calculate computed parameters
    ! We first pick dt so that the resulting r will ensure convergence,
    ! then calculate how many time steps we need (Nt) to reach required end time.
    dt = r / (Nx * Nx * alpha)
    Nt = ceiling(t_end / dt)

    if (mpi_rank == 0) then
        write(*,"(A,I0,A,F10.4,A,F10.8,A,F10.8)") "Input parameters: Nx=", Nx, &
                " t_end=", t_end, " alpha=", alpha, " r=", r
        write(*,"(A,F10.4,A,I0)") "Computed parameters: dt=", dt, " Nt=", Nt
    end if

    ! Work out which x-partition this rank will be calculating
    quotient = (Nx - 1) / mpi_size
    remainder = mod(Nx - 1, mpi_size)

    i_start = mpi_rank * quotient + min(mpi_rank, remainder)
    i_end = (mpi_rank + 1) * quotient + 1 + min(mpi_rank + 1, remainder)
    i_width = i_end - i_start + 1 ! NB: This includes left & right overlaps

    ! Print out partition info for debugging purposes
    write(*,"(A,I0,A,I0,A,I0,A,I0,A,I0,A,I0,A)") "Rank: ", mpi_rank, &
        " i_start=", i_start, " i_end=", i_end, " i_width=", i_width, &
        " (q=", quotient, ", r=", remainder, ")"

    ! Allocate memory arrays to hold current & next time stemp iteration data for this rank
    allocate(work1(i_width))
    allocate(work2(i_width))

    ! We will first use curr_values to store the current (initial, j=0)
    ! time step data, and next_values for the next (j=1) data.
    curr_values => work1
    next_values => work2

    ! Set up initial conditions
    ! We will first use curr_values to store the current (initial, j=0) time step data,
    ! and next_values for the next (j=1) data.
    do ki = 1, i_width
        i = i_start + ki
        curr_values(ki) = initial_condition(dble(i-1) / Nx)
    end do

    call debug_values("INITIAL values", mpi_rank, curr_values, i_width)

    ! Now we do the main calculation. This is a nested loop,
    ! iterating over time (j) first, then over x values (i).
    do j = 1, Nt
        ! Compute and fill in next array of values for this time step.
        ! The new values are saved into the next_values array.
        do ki = 2, i_width - 1
            diff = curr_values(ki+1) - 2 * curr_values(ki) + curr_values(ki-1)
            next_values(ki) = curr_values(ki) + r * diff
        end do

        ! Preserve boundary conditions (on first & last processes)
        if (mpi_rank == 0) then
            next_values(1) = curr_values(1)
        end if
        if (mpi_rank == mpi_size - 1) then
            next_values(i_width) = curr_values(i_width)
        end if

        ! We now perform a "halo exchange" to send our new endpoint values
        ! to adjacent MPI ranks.
        !
        ! Note in the following we're using non-blocking MPI_Issend() calls
        ! to avoid potential for deadlocks.
        !
        ! Part 1: Pass left endpoint results leftwards
        if (mpi_rank > 0) then
            call MPI_Issend(next_values(2), 1, MPI_DOUBLE, mpi_rank-1, mpi_tag, MPI_COMM_WORLD, my_mpi_request, ierr)
            call MPI_Wait(my_mpi_request, my_mpi_status, ierr)
        end if
        if (mpi_rank < mpi_size - 1) then
            call MPI_Recv(next_values(i_width), 1, MPI_DOUBLE, mpi_rank+1, mpi_tag, MPI_COMM_WORLD, my_mpi_status, ierr)
        end if
        ! Part 2: Pass right endpoint results rightwards
        if (mpi_rank < mpi_size - 1) then
            call MPI_Issend(next_values(i_width - 1), 1, MPI_DOUBLE, mpi_rank+1, mpi_tag, MPI_COMM_WORLD, my_mpi_request, ierr)
            call MPI_Wait(my_mpi_request, my_mpi_status, ierr)
        end if
        if (mpi_rank > 0) then
            call MPI_Recv(next_values(1), 1, MPI_DOUBLE, mpi_rank-1, mpi_tag, MPI_COMM_WORLD, my_mpi_status, ierr)
        end if

        ! Next we swap curr_values & next_values for the next iteration
        temp => curr_values
        curr_values => next_values
        next_values => temp
    end do

    call debug_values("FINAL values  ", mpi_rank, curr_values, i_width)

    if (mpi_rank > 0) then
        ! Worker process. Send results to manager
        write(*, "(A,I0,A)") "Rank ", mpi_rank, " sending final results to manager"
        call MPI_Ssend(curr_values, i_width, MPI_DOUBLE, 0, mpi_tag, MPI_COMM_WORLD, ierr)
    else
        ! Manager process. Build (reduce) final result array.
        !
        ! Exercise for reader: rewrite the code below
        ! to print out the result without first building a
        ! final result array :-)
        !
        allocate(result(Nx + 1))

        ! First fill in own partial results, which will fill the
        ! first segment of the resulting result array.
        ! [Making the manager == rank 0 helps here!]
        i = 1
        do ki = 1, i_width
            result(i) = curr_values(ki)
            i = i + 1
        end do
        i = i - 2; ! to account for halo - next rank overlaps by 2

        ! Then merge in results from other ranks. We'll do these
        ! in exact order, as that's a bit easier.
        do k = 1, mpi_size - 1
            write(*,"(A,I0)") "Manager receiving final results from rank ", k
            call MPI_Recv(result(i), Nx + 2 - i, MPI_DOUBLE, k, mpi_tag, MPI_COMM_WORLD, my_mpi_status, ierr)
            call MPI_Get_count(my_mpi_status, MPI_DOUBLE, recv_count, ierr)
            write(*,"(A,I0,A,I0)") "Manager received ", recv_count, " items from rank ", k
            i = i + recv_count - 2 ! -2 is to account for halo, as above
        end do

        ! Output final results
        write(*,"(A)") "T(.,t) values at final time step:"
        resultptr => result
        call print_values(resultptr)
    end if

    ! Clean up and exit
    call MPI_Finalize(ierr)

end program heat_equation


! This function defines our initial condition.
! We'll use a simple step function for this.
double precision function initial_condition (x) result (T)
    implicit none
    double precision :: x

    if ((x >= 0.4d0) .and. (x <= 0.6d0)) then
        T = 1.0d0
    else
        T = 0.2d0
    end if

end function initial_condition


! Reads in the user-specified command line arguments
! and sets the appropriate globals as appropriate.
!
! Sets valid to .TRUE. if all arguments have been specified
! correctly, otherwise .FALSE.
subroutine read_parameters(Nx, t_end, valid)
    implicit none
    integer, intent(out) :: Nx
    double precision, intent(out) :: t_end
    logical, intent(out) :: valid
    character(len=12) :: arg

    valid = .FALSE.
    if (command_argument_count() /= 2) then
        write(0,"(A)") "Required arguments: <x_partitions(Nx)> <t_end>"
        return
    end if
    call get_command_argument(1, arg)
    read(arg,*) Nx
    if (Nx < 1) then
        write(0,"(A)") "Number of x partitions must be >= 1"
        return
    end if
    call get_command_argument(2, arg)
    read(arg,*) t_end
    if (t_end <= 0.0d0) then
        write(0,"(A)") "Time endpoint must be positive"
        return
    end if
    valid = .TRUE.

end subroutine read_parameters


! Prints the given array of values for a particular time step
subroutine print_values(values)
    implicit none
    double precision, dimension(:), pointer :: values
    integer :: i

    write(*,"(A)", advance="no") "["
    do i = 1, size(values)
        write(*,"(F12.10)", advance="no") values(i)
        if (i < size(values)) then
            write(*,"(A)", advance="no") ", "
        end if
    end do
    write(*,"(A)") "]"

end subroutine print_values


! Helper for debugging rank-specific values
!
! NOTE: This first formats the final result as a string, which is then
! output as a single unit. This helps prevent data from multiple MPI
! processes getting mixed up. However it makes the code below fairly
! yucky looking!
subroutine debug_values(heading, mpi_rank, values, count)
    implicit none
    character(len=*) :: heading
    integer :: mpi_rank
    double precision, dimension(:), pointer :: values
    integer :: count
    character(len=len(heading) + 15 + count * 14) :: buffer ! Buffer to build up formatted value data for output
    integer :: i ! Value counter
    integer :: j ! Index within buffer

    j = len(heading)
    write(buffer(1:j),"(A)") heading
    write(buffer(j+1:j+12), "(A,I3,A)") " rank ", mpi_rank, ": ["
    j = j + 13
    do i = 1, count
        if (i == 1 .or. i == count) then
            write(buffer(j:j),"(A)") "("
            j = j + 1
        end if
        write(buffer(j:j+11),"(F12.10)") values(i)
        j = j + 12
        if (i == 1 .or. i == count) then
            write(buffer(j:j),"(A)") ")"
            j = j + 1
        end if
        if (i < count) then
            write(buffer(j:j+1), "(A)") ", "
            j = j + 2
        end if
    end do
    write(buffer(j:j), "(A)") "]"
    write(*,"(A)") buffer(1:j)

end subroutine debug_values
