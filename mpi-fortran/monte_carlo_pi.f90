! Estimation of Pi via Monte Carlo simulation.
!
! MPI Fortran version. Make sure you've first looked at the
! serial version of this code !and! the MPI version of
! hello_world.c.
!
!
! How we are parallelising this problem
! =====================================
!
! We simply get each individual MPI process to do
! a (roughly) equal share of the total number of requested
! trials and record the number of successful Monte Carlo
! trials (success_count).
!
! Once each process has finished its work, it sends its
! success_count to process with rank 0 which, as is common,
! acts as the "manager" process to coordinate all of the work.
!
! The manager process receives all of the success_counts
! from the other "worker" processes and adds them up to
! calculate the total number of successes recorded over all
! processes.
!
! It can then trivially compute the estimate of Pi in exactly
! the same way as the serial code.
!
program monte_carlo_pi
    use mpi
    implicit none
    integer :: mpi_size, mpi_rank ! MPI world size & rank
    integer, parameter :: tag = 0 ! We can get away with using a single tag for all MPI communication here
    integer :: ierr ! Used to check for errors in MPI calls
    character(len=20) :: arg
    integer :: num_trials_total ! Total number of Monte Carlo trials to perform
    integer :: num_trials_this_process ! Number of MC trials to be done by this process
    integer :: remainder ! Remainder when dividing num_trials_total by MPI world size
    integer :: success_count ! Number of MC successes recorded by this process
    integer :: total_success_count ! Total number of successes (computer by maneger process only)
    integer :: received_success_count ! Used by manager process to receive success counts from workers (manager only)
    double precision :: pi_estimate ! Final Pi estimate (computed by manager proces only)
    integer :: i

    ! Initialize the MPI environment.
    call MPI_Init(ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, mpi_size, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, mpi_rank, ierr)

    ! Read requested number of trials from command line arguments
    if (command_argument_count() /= 1) then
        write(0,"(A)") "Required argument: <number of trials>"
        call MPI_Finalize(ierr)
        stop 1
    end if
    call get_command_argument(1, arg)
    read(arg,*) num_trials_total
    if (num_trials_total <= 0) then
        write(0,"(A)") "Number of trials must be a positive integer"
        call MPI_Finalize(ierr)
        stop 1
    end if

    ! Work out the number of trials to do per process.
    ! This is computed with a bit of integer division, which
    ! of course rounds downward, so we perform 1 extra trial on some of the
    ! ranks to make up the remainder.
    num_trials_this_process = num_trials_total / mpi_size
    remainder = mod(num_trials_total, mpi_size)
    if (mpi_rank < remainder) then
        num_trials_this_process = num_trials_this_process + 1
    end if

    ! "Seed" the random number generator so that each rank
    ! generates a different sequence of random numbers
    call seed_random_numbers(mpi_rank)

    ! Run specified number of Monte Carlo trials
    call do_monte_carlo(num_trials_this_process, success_count)

    ! Report the partial result for this rank, which is handy for
    ! understanding what's going on.
    write(*,"(A,I0,A,I0,A,I0)") "Partial result for rank=", mpi_rank, &
        ", num_trials=", num_trials_this_process, &
        ", success_count=", success_count

    if (mpi_rank /= 0) then
        ! This is a worker process.
        ! We send our partial result to the manager process (0)
        !
        ! We use MPI_Ssend() for this, which is the simplest way
        ! of sending an MPI message.
        ! It sends the message immediately (=synchronous),
        ! then waits until the other process receives the
        ! message (=blocking).
        !
        ! This simple method works fine in this example, but you need
        ! to be careful in more complex cases as you can end up with a
        ! deadlock.
        call MPI_Ssend(success_count, 1, MPI_INTEGER, 0, tag, MPI_COMM_WORLD, ierr)
    else
        ! This is the manager process.
        ! We will receive results from each of the worker processes
        ! and tot up our final result (success_count_total), which
        ! we'll start off as the number of successes recorded by this
        ! process.
        total_success_count = success_count
        do i=1, mpi_size-1
            ! Receive next incoming worker process result
            ! into received_success_count variable.
            ! We use MPI_ANY_SOURCE here to indicate we're happy to
            ! receive results from any rank in whichever order to come in -
            ! everything should tally up in the end.
            call MPI_Recv(received_success_count, 1, MPI_INTEGER, MPI_ANY_SOURCE, &
                    tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr)
            ! Add the received count to the final total
            total_success_count = total_success_count + received_success_count
        end do
        ! Compute our resulting estimate for pi
        pi_estimate = 4.0d0 * total_success_count / num_trials_total

        ! Output final result
        write(*,"(A,F12.10,A,I0,A,I0)") "pi=~", pi_estimate, &
            " num_trials=", num_trials_total, &
            " success_count=", total_success_count
    end if

    ! Clean up and exit
    call MPI_Finalize(ierr)

end program monte_carlo_pi


! Runs the Monte Carlo simulation for the given number
! of trials, returning the number of successful samples.
subroutine do_monte_carlo(num_trials, success_count)
    implicit none
    integer, intent(in) :: num_trials
    integer, intent(out) :: success_count
    integer :: i
    real :: x,y

    success_count = 0
    do i = 1, num_trials
        call random_number(x)
        call random_number(y)
        if ((x - 0.5)**2 + (y - 0.5)**2 < (0.5)**2) then
            success_count = success_count + 1
        end if
    end do

end subroutine do_monte_carlo


! Seeds the random number generator for this process
! using the current clock time and the current MPI rank.
!
! This (compact but probably rubbish) method used below is based on:
! https://gcc.gnu.org/onlinedocs/gcc-4.6.4/gfortran/RANDOM_005fSEED.html
! You'd probably want to use something better in real life :-)
subroutine seed_random_numbers(mpi_rank)
    integer :: mpi_rank
    integer, dimension(:), allocatable :: random_seed_data
    integer :: random_seed_size
    integer :: clock_time

    call random_seed(size=random_seed_size)
    allocate(random_seed_data(random_seed_size))
    call system_clock(count=clock_time)
    random_seed_data = clock_time + 37 * (/ (i + mpi_rank, i = 1, random_seed_size) /)
    call random_seed(put=random_seed_data)

end subroutine seed_random_numbers
