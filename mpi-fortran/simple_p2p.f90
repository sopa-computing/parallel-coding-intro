! Example of simple point 2 point communication.
!
! This code is intended to run with 2 MPI processes.
! Rank 0 sends a message to rank 1, and rank 1 receives this message.
program simple_p2p
    use mpi
    implicit none
    integer, parameter :: tag = 0 ! We can get away with using a single tag for all MPI communication here
    integer :: mpi_size, mpi_rank ! MPI world size & rank
    integer :: ierr ! Used to check for errors in MPI calls
    integer :: to_send
    integer :: received

    ! Initialize the MPI environment.
    ! This should be first thing you do in any MPI program.
    call MPI_Init(ierr)

    ! Get details about this process (mpi_rank)
    ! and the total number of processes (mpi_size)
    call MPI_Comm_size(MPI_COMM_WORLD, mpi_size, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, mpi_rank, ierr)

    ! Sanity check MPI size - this example needs 2 parallel processes
    if (mpi_size /= 2) then
        write(0,"(A)") "This example is intended to run with 2 parallel processes."
        write(0,"(A)") "Please relaunch with -np 2."
        call MPI_Finalize(ierr)
        stop 1
    end if

    ! ============ MPI communication fun starts here ==============

    if (mpi_rank == 0) then
        ! We will send a simple message (the number 42) to rank 1.
        ! We use the simplest MPI_Ssend() function for this, which blocks
        ! until the message gets received
        to_send = 42;
        write(*,"(A,I0,A)") "Rank 0 is calling MPI_SSend() to send a message ", to_send, "  to rank 1"
        call MPI_Ssend(to_send, 1, MPI_INTEGER, 1, tag, MPI_COMM_WORLD, ierr)
        write(*,"(A)") "Rank 0 has successfully completed the blocking MPI_Ssend() call"
    elseif (mpi_rank == 1) then
        ! We will receive the message that rank 0 is sending us.
        ! We use the simplest MPI_Recv() function for this, which blocks
        ! until the message gets received
        write(*,"(A)") "Rank 1 is calling MPI_Recv() to receive a message from rank 0"
        call MPI_Recv(received, 1, MPI_INTEGER, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr)
        write(*,"(A,I0)") "Rank 1 has successfully completed the blocking MPI_Recv() call and received message ", received
    end if

    ! ============ MPI communication fun ends here ==============

    ! Shut down the MPI environment.
    call MPI_Finalize(ierr)

end program simple_p2p
