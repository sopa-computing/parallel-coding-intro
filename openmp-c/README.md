# OpenMP C codes

This directory contains the OpenMP versions of our example codes,
written in C.

If you prefer Fortran, Fortran versions of this code can be found in the
adjacent **openmp-fortran** folder.

## Details about these codes

* **hello\_world.c**: OpenMP version of serial Hello World example code
* **monte\_carlo\_pi.c**: Initial OpenMP version of serial Monte Carlo Pi example code
* **monte\_carlo\_pi\_v2\_atomic.c**: Alternate OpenMP version of **monte\_carlo\_pi.c** using
  an atomic instead of a critical section.
* **monte\_carlo\_pi\_v3\_reduction.c**: Alternate OpenMP version of **monte\_carlo\_pi.c** using
  a reduction to compute the final total number of trials.
* **heat\_equation.c**: OpenMP version of serial Heat Equation example code

## How to compile the code

The code is written in C so first needs to be **compiled** into runnable
binary code. I have provided a **Makefile** to do this for you. (A Makefile is
essentially an old-fashioned and hard-to-read recipe for building code projects.)
You can invoke this by typing:

```sh
make
```

Note that you need to run this from the directory containing this file.

This Makefile should work on any Linux computer with the GCC compiler suite,
and it includes some alternative instructions for compiling the code using the
Intel compiler suite, which is available on School Linux computers and on Eddie.
If you're familiar with building C code, it should be possible to tweak the
Makefile to make the code work with other compilers or on MacOS or Windows
computers.

## How to run the code

Once compiled, you can run the codes using 4 OpenMP threads
by typing in:

```sh
OMP_NUM_THREADS=4 ./hello_world

OR

OMP_NUM_THREADS=4 ./monte_carlo_pi 1000000

OR

OMP_NUM_THREADS=4 ./heat_equation 100 10
```

Note that the **monte_carlo_pi** and **heat_equation** codes require you to
supply one or two parameters. You'll get told what information you need
if you simply try to run them without parameters.
