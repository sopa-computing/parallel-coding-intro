#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

/* Numerical approximation of the heat equation, using
 * discrete finite difference methods.
 * OpenMP implementation.
 *
 * Please see the serial version for background information
 * and an explanation of the key variables.
 *
 * NOTE: This algorithm does not scale very well at all!
 */

// Horizontal partition size to use when splitting up the
// x-axis for processing by different threads. We pick this
// to be a round number (in terms of powers of two) and
// large enough to help ensure that adjacent threads are not
// reading and writing to nearby memory at the same time.
#define PARTITION_SIZE 1024

// User-specified parameters
static long Nx; // Number of x sub-intervals to pick from interval [0,1]
static double t_end; // Final time to reach

// The following parameters are currently hard-coded
static const double alpha = 0.001; // alpha coefficient in heat equation
static const double r = 0.25; // Must be <1/2 for convergence

// Declare functions defined further down
static double initial_condition(double x);
static int read_parameters(int argc, char *argv[]);
static void print_values(double *values);

int main(int argc, char *argv[]) {
    double dt; // Time increment (computed from params)
    long Nt; // Number of time iterations required (computed from params)
    long i, j; // i = discrete 'x' point (0..Nx), j = discrete 't' point (0..Nt)
    double *curr_values, *next_values; // Memory arrays for storing current & next time step values
    double *temp; // Used for swapping buffers
    double diff; // Temporary variable used during inner calculation loop

    // Read in user-specified parameters
    if (!read_parameters(argc, argv)) {
        return EXIT_FAILURE;
    }

    // Calculate computed parameters.
    // We first pick dt so that the resulting r will ensure convergence,
    // then calculate how many time steps we need (Nt) to reach required end time.
    dt = r / (Nx * Nx * alpha);
    Nt = (long) ceil(t_end / dt);

    printf("Input parameters: Nx=%ld t_end=%f alpha=%e r=%f\n", Nx, t_end, alpha, r);
    printf("Computed parameters: dt=%e Nt=%ld\n", dt, Nt);

    // Allocate memory arrays to hold iteration data
    curr_values = malloc(sizeof(double) * (1 + Nx));
    next_values = malloc(sizeof(double) * (1 + Nx));

    // Set up initial conditions
    // We will first use curr_values to store the current (initial, j=0) time step data,
    // and next_values for the next (j=1) data.
    for (i = Nx; i >= 0; --i) {
        curr_values[i] = initial_condition((double) i / Nx);
    }

    // Fix boundary conditions for the first (and subsequent) iterations
    next_values[0] = curr_values[0];
    next_values[Nx] = curr_values[Nx];

    // The following initiates a team of parallel threads;
    // to execute the next block (i.e. {...}) of code
    #pragma omp parallel default(none) \
    shared(curr_values, next_values, Nx, Nt, stderr) \
    private(i, j, diff, temp)
    {
        // First perform a quick sanity check that the algorithm below makes sense for
        // the chosen Nx value & value of OMP_NUM_THREADS.
        int thread_id = omp_get_thread_num();
        int num_threads = omp_get_num_threads();
        if (Nx < (num_threads - 1) * PARTITION_SIZE && thread_id == 0) {
            fputs("WARNING: Nx is too small for the current algorithm to keep all threads occupied!\n", stderr);
        }

        for (j = 1; j <= Nt; ++j) {
            // Compute and fill in next array of values for
            // this time step.
            //
            // This is done as parallel for loop. Each thread
            // will execute part of the loop. (static,PARTITION_SIZE)
            // specifies that one thread will do indices 0 -> PARTITION_SIZE-1,
            // another will do PARTITION_SIZE -> PARTITION_SIZE - 1 etc.
            #pragma omp for schedule(static,PARTITION_SIZE)
            for (i = 1; i < Nx; ++i) {
                diff = curr_values[i+1] - 2 * curr_values[i] + curr_values[i-1];
                next_values[i] = curr_values[i] + r * diff;
            }
            // There is an implicit barrier at the end of 'omp for' loop,
            // so all threads will wait here until everyone has completed
            // the loop.
            //
            // Next we need to prepare for the next outer iteration.
            // This work should only be done once otherwise things will get in a
            // terrible mess, so we use the 'single' directive
            // to limit the work to one thread. The others will wait until
            // this work has been done as there's an implicit barrier afterwards.
            #pragma omp single
            {
                // Next we swap curr_values & next_values for the next iteration
                temp = curr_values;
                curr_values = next_values;
                next_values = temp;
            }
            // All threads will get here at the same point, so we can now
            // proceed to the next iteration.
        }
        // We've reached the end of the outer loop and now
        // exit the parallel section and go back to being serial again.
    }

    // Output final results
    puts("T(.,t) values at final time step:");
    print_values(curr_values);

    // Clean up and exit
    free(curr_values);
    free(next_values);
    return EXIT_SUCCESS;
}

/* This function defines our initial condition.
 * We'll use a simple step function for this.
 */
double initial_condition(double x) {
    return (x >= 0.4 && x <= 0.6) ? 1 : 0.2;
}

/* Reads in the user-specified command line arguments
 * and sets the appropriate globals as appropriate.
 *
 * Returns non-zero if all arguments have been specified
 * correctly, otherwise zero.
 */
int read_parameters(int argc, char *argv[]) {
    if (argc != 3) {
        fputs("Required arguments: <x_partitions(Nx)> <t_end>\n", stderr);
        return 0;
    }
    Nx = atol(argv[1]);
    if (Nx < 1) {
        fputs("Number of x partitions must be >= 1\n", stderr);
        return 0;
    }
    t_end = atof(argv[2]);
    if (t_end <= 0.0) {
        fputs("Time endpoint must be positive\n", stderr);
        return 0;
    }
    return 1;
}

/* Prints the given array of values for a particular time step */
void print_values(double *values) {
    long i;

    printf("[");
    for (i = 0; i <= Nx; ++i) {
        printf("%.10f", values[i]);
        if (i < Nx) printf(", ");
    }
    printf("]\n");
}
