#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/**
 * Hello World, C style!
 *
 * Alternate OpenMP version of hello_world.c that moves variable
 * declarations into the parallel sections and ends up looking a bit simpler.
 *
 * This simply gets each OpenMP thread to independently say Hello World,
 * and report its thread ID/number.
 */
int main(void) {
    // We use '#pragma omp parallel' to start a parallel code section.
    // The curly brackets delimit the start and end of the parallel section.
    //
    // We use 'default(none)' here like in hello_world.c, but we've
    // deliberately moved the variable declarations *into* the parallel
    // section, which is another way of making them private to each thread.
    #pragma omp parallel default(none)
    {
        // Get information about this thread, and the total number of
        // threads in the current team.
        int thread_id = omp_get_thread_num(); // Thread number/id for "this" thread
        int num_threads = omp_get_num_threads(); // Number of OpenMP threads

        // Embellish our original "Hello World" message to report
        // the thread number and team size
        printf("Hello World! (This is thread %d out of %d)\n",
               thread_id, num_threads);
    }
    // We've now exited the parallel section so return to normal serial
    // execution. So there will only be one thread left running at this point.
    // The other threads in the team will either get put to sleep or deleted.

    // Exit as normal
    return EXIT_SUCCESS;
}
