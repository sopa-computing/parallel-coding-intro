#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

/**
 * Estimation of Pi via Monte Carlo simulation.
 *
 * Alternate OpenMP version using 'atomic' instead of 'critical'.
 */

// Declare functions defined further below
static long do_monte_carlo(long num_trials, struct random_data *buf);
static double next_random(struct random_data *buf);

#define RANDOM_STATE_SIZE 32

int main(int argc, char *argv[]) {
    long total_num_trials; // Total number of Monte Carlo trials to perform
    long total_success_count = 0; // Total number of MC successes recorded
    double pi_estimate; // Final Pi estimate

    // Read requested number of trials from command line arguments
    if (argc != 2) {
        fputs("Required argument: <number of trials>\n", stderr);
        return EXIT_FAILURE;
    }
    total_num_trials = atol(argv[1]);
    if (total_num_trials <= 0) {
        fputs("Number of trials must be a positive long integer\n", stderr);
        return EXIT_FAILURE;
    }

    // Start parallel work
    //
    // Note how we're sharing total_num_trials (as each thread is going
    // to read it) and total_success_count (as each thread is going to update it).
    //
    // Note that I've decided to create local variables within this block.
    // Each thread will create its own set of variables, so they'll be private.
    // I could also have created them at the top of the main() function and
    // declared them as 'private'.
    #pragma omp parallel default(none) \
            shared(total_success_count, total_num_trials)
    {
        int thread_id; // Thread number/id for "this" thread
        int num_threads; // Number of OpenMP threads running here
        int remainder; // Used to help compute number of trials to perform
        long num_trials_this_thread; // Number of trials to be done by this thread
        long success_count_this_thread; // Number of successes recorded by this thread
        char statebuf[RANDOM_STATE_SIZE]; // Raw buffer to help generate random numbers
        struct random_data buf = { NULL }; // Buffer used to generate random numbers for this thread

        // Work out how many trials this thread should do.
        // It's the same logic as the MPI example, effectively an even workload split
        // with a bit of fudging if the numbers don't divide cleanly.
        thread_id = omp_get_thread_num();
        num_threads = omp_get_num_threads();
        num_trials_this_thread = total_num_trials / num_threads;
        remainder = total_num_trials % num_threads;
        if (thread_id < remainder) {
            ++num_trials_this_thread;
        }

        // Set up private random state for this thread
        // We need to do this as the default random
        initstate_r(time(NULL) + thread_id, statebuf, sizeof(statebuf), &buf);

        // Now perform the required number of Monte Carlo trials for this thread
        success_count_this_thread = do_monte_carlo(num_trials_this_thread, &buf);
        printf("Partial result for thread=%d: num_trials=%ld success_count=%ld\n",
               thread_id, num_trials_this_thread, success_count_this_thread);

        // Finally each thread adds the number of successful samples it recorded
        // to the overall total.
        //
        // There's a potential race condition here, so we use 'omp atomic'
        // to make sure that that the addition is performed safely.
        #pragma omp atomic update
        total_success_count += success_count_this_thread;
    }
    // End of the parallel work. So there's only one thread now doing anything.

    // Compute the final estimate for Pi
    pi_estimate = 4.0 * total_success_count / total_num_trials;

    // Output final result
    printf("Final result: pi=~%0.8f num_trials=%ld success_count=%ld\n",
            pi_estimate, total_num_trials, total_success_count);

    // Exit happily
    return EXIT_SUCCESS;
}

/**
 * We define a wee helper macro to square a number. This is used
 * in do_monte_carlo() below to make things a bit easier to read.
 */
#define SQUARE(x) ((x)*(x))

/**
 * Runs the Monte Carlo simulation for the given number
 * of trials, returning the number of successful samples.
 *
 * This function also takes a pointer to a thread-private
 * buffer for generating random data.
 */
long do_monte_carlo(long num_trials, struct random_data *buf) {
    long i, success_count;
    double x, y;

    success_count = 0;
    for (i = num_trials; i > 0; --i) {
        x = next_random(buf);
        y = next_random(buf);
        if (SQUARE(x-0.5) + SQUARE(y-0.5) < SQUARE(0.5)) {
            ++success_count;
        }
    }
    return success_count;
}

/**
 * Returns a pseudo-random number in the range [0,1),
 * number using the given buffer to track the random state.
 *
 * NOTE: You would probably want to use a better random
 * number generator than this!
 */
double next_random(struct random_data *buf) {
    int32_t rnd;
    random_r(buf, &rnd);
    return (double) rnd / RAND_MAX;
}
