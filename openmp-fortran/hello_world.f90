! Hello World, Fortran style!
!
! OpenMP version. This simply gets each OpenMP thread to
! independently say Hello World, and report its number.
program hello_world
    use omp_lib
    implicit none
    integer :: thread_id ! Thread number/id for "this" thread
    integer :: num_threads ! Number of OpenMP threads

    ! We use '!$OMP PARALLEL' to start a parallel code section.
    ! The curly brackets delimit the start and end of the parallel section.
    !
    ! 'DEFAULT(none)' indicates that all variables used within the
    ! parallel section need to be explicitly declared as either 'private'
    ! or 'shared'. If you leave this out, variables are assumed to be
    ! 'shared' by default, which is now considered to have been a
    ! mistake in the initial OpenMP design. It's recommended that you
    ! always use 'default(none)'.
    !
    ! private(num_threads, thread_id) indicates that each thread will
    ! get its own *private* copy of these variables. That makes sense,
    ! as thread_id is going to be different for each thread.
    !
    !$OMP PARALLEL DEFAULT(none) PRIVATE(num_threads, thread_id)

    ! Get information about this thread, and the total number of
    ! threads in the current team.
    thread_id = omp_get_thread_num()
    num_threads = omp_get_num_threads()

    ! Embellish our original "Hello World" message to report
    ! the thread number and team size
    write(*,"(A,I0,A,I0,A)") "Hello World! (This is thread ", thread_id, &
        " out of ", num_threads, ")"

    !$OMP END PARALLEL
    !
    ! We've now exited the parallel section so return to normal serial
    ! execution. So there will only be one thread left running at this point.
    ! The other threads in the team will either get put to sleep or deleted.
end program hello_world
