! Estimation of Pi via Monte Carlo simulation.
!
! Alternate OpenMP version using 'atomic' instead of 'critical'.
program monte_carlo_pi
    use omp_lib
    implicit none
    character(len=20) :: arg
    integer :: total_num_trials ! Number of Monte Carlo trials to perform
    integer :: total_success_count ! Number of MC successes recorded
    double precision :: pi_estimate; ! Final Pi estimate

    integer :: thread_id ! Thread number/id for "this" thread
    integer :: num_threads ! Number of OpenMP threads running here
    integer :: remainder ! Used to help compute number of trials to perform
    integer :: num_trials_this_thread ! Number of trials to be done by this thread
    integer :: success_count_this_thread ! Number of successes recorded by this thread

    ! Read requested number of trials from command line arguments
    if (command_argument_count() /= 1) then
        write(0,"(A)") "Required argument: <number of trials>"
        stop 1
    end if
    call get_command_argument(1, arg)
    read(arg,*) total_num_trials
    if (total_num_trials <= 0) then
        write(0,"(A)") "Number of trials must be a positive integer"
        stop 1
    end if

    ! Start parallel work
    !
    ! This version of the example uses 'REDUCTION(+:total_success_count)'.
    ! This magically creates a private copy of total_success_count for each
    ! thread, then safely adds up all of the individual values at the end
    ! of the parallel section to compute the total for all threads.
    !
    ! As in the previous examples, I've decided to create local variables
    ! within this block. Each thread will create its own set of variables,
    ! so they'll be private. I could also have created them at the top of
    ! the main() function and declared them as 'private'.
    ! declared them as 'private'.
    !$OMP PARALLEL DEFAULT(none) &
    !$OMP& SHARED(total_success_count, total_num_trials) &
    !$OMP& PRIVATE(thread_id, num_threads, remainder, num_trials_this_thread, success_count_this_thread)

    ! Work out how many trials this thread should do.
    ! It's the same logic as the MPI example, effectively an even workload split
    ! with a bit of fudging if the numbers don't divide cleanly.
    thread_id = omp_get_thread_num()
    num_threads = omp_get_num_threads()
    num_trials_this_thread = total_num_trials / num_threads
    remainder = mod(total_num_trials, num_threads)
    if (thread_id < remainder) then
        num_trials_this_thread = num_trials_this_thread + 1
    end if

    ! Now perform the required number of Monte Carlo trials for this thread
    call do_monte_carlo(num_trials_this_thread, success_count_this_thread)
    write(*,"(A,I0,A,I0,A,I0)") "Partial result for thread=", thread_id, &
        " num_trials=", num_trials_this_thread, &
        " success_count=", success_count_this_thread

    ! Finally each thread adds the number of successful samples it recorded
    ! to the overall total.
    !
    ! There's a potential race condition here, so we use 'omp atomic'
    ! to make sure that that the addition is performed safely.
    !$OMP ATOMIC UPDATE
    total_success_count = total_success_count + success_count_this_thread
    !$OMP END ATOMIC

    !$OMP END PARALLEL
    ! End of the parallel work. So there's only one thread now doing anything.

    ! Compute our resulting estimate for pi
    pi_estimate = 4.0d0 * total_success_count / total_num_trials

    ! Output final result
    write(*,"(A,F12.10,A,I0,A,I0)") "Final result: pi=~", pi_estimate, &
            " total_num_trials=", total_num_trials, &
            " total_success_count=", total_success_count

end program monte_carlo_pi


! Runs the Monte Carlo simulation for the given number
! of trials, returning the number of successful samples.
subroutine do_monte_carlo(num_trials, success_count)
    implicit none
    integer, intent(in) :: num_trials
    integer, intent(out) :: success_count
    integer :: i
    real :: x,y

    success_count = 0
    do i = 1, num_trials
        call random_number(x)
        call random_number(y)
        if ((x - 0.5)**2 + (y - 0.5)**2 < (0.5)**2) then
            success_count = success_count + 1
        end if
    end do

end subroutine do_monte_carlo
