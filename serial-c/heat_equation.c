#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * Numerical approximation of the heat equation, using
 * discrete finite difference methods.
 * Serial code implementation.
 *
 * This uses techniques detailed in various sources, including:
 * http://www.nada.kth.se/~jjalap/numme/FDheat.pdf
 *
 * Here we're trying to solve: dT(x,t)/dt = alpha d^2T/dx^2
 * subject to:
 *
 * - 0 <= x <= 1
 * - An initial condition T(x,0)
 * - Boundary conditions T(0,t)=T(0,0) & T(1,t)=T(1,0)
 *
 * x & t are mapped to discrete points on a uniform mesh,
 * represented by integers i & j respectively.
 *
 * We let Nx = the number of horizontal sub-intervals of [0,1] to use.
 * So x = i / Nx for 0 <= i <= Nx.
 *
 * Numeric approximated is then approximated with:
 *
 * T(i,j+1) = T(i,j) + r [T(i-1,j) - 2T(i,j) + T(i+1,j)]
 *
 * where r = alpha * dt / (dx)^2.
 *
 * We choose dt so that r << 1/2, necessary for convergence of solution.
 *
 * Then we calculate Nt = number of time steps required to get us to
 * our desired end time. Here t = j dt for 0 <= j <= Nt.
 *
 *
 * Algorithm notes:
 *
 * - We calculate this one time step at a time. So we start by computing
 * the T(i,1) values for each i, then move to T(i,2) and so on. We stop
 * after the required number of time steps.
 *
 * - This code implementation only stores vectors (arrays) of T(i,j)
 * values for the current (j) iterations. It uses one memory array
 * to store these values, plus a second array to build up the values for
 * the next (j+1) time step. It alternates which array it's updating
 * in each iteration, which is more efficient than copying data between
 * buffers.
 */

// User-specified parameters
static long Nx; // Number of x sub-intervals to pick from interval [0,1]
static double t_end; // Final time to reach

// The following parameters are currently hard-coded
static const double alpha = 0.001; // alpha coefficient in heat equation
static const double r = 0.25; // Must be <1/2 for convergence

// Declare functions defined further down
static double initial_condition(double x);
static int read_parameters(int argc, char *argv[]);
static void print_values(double *values);

int main(int argc, char *argv[]) {
    double dt; // Time increment (computed from params)
    long Nt; // Number of time iterations required (computed from params)
    long i, j; // i = discrete 'x' point (0..Nx), j = discrete 't' point (0..Nt)
    double *curr_values, *next_values; // Memory arrays for storing current & next time step values
    double *temp; // Used for swapping buffers
    double diff; // Temporary variable used during inner calculation loop

    // Read in user-specified parameters
    if (!read_parameters(argc, argv)) {
        return EXIT_FAILURE;
    }

    // Calculate computed parameters.
    // We first pick dt so that the resulting r will ensure convergence,
    // then calculate how many time steps we need (Nt) to reach required end time.
    dt = r / (Nx * Nx * alpha);
    Nt = (long) ceil(t_end / dt);

    printf("Input parameters: Nx=%ld t_end=%f alpha=%e r=%f\n", Nx, t_end, alpha, r);
    printf("Computed parameters: dt=%e Nt=%ld\n", dt, Nt);

    // Allocate memory arrays to hold iteration data
    curr_values = malloc(sizeof(double) * (1 + Nx));
    next_values = malloc(sizeof(double) * (1 + Nx));

    // Set up initial conditions
    // We will first use curr_values to store the current (initial, j=0) time step data,
    // and next_values for the next (j=1) data.
    for (i = Nx; i >= 0; --i) {
        curr_values[i] = initial_condition((double) i / Nx);
    }

    // Fix boundary conditions for the first (and subsequent) iterations
    next_values[0] = curr_values[0];
    next_values[Nx] = curr_values[Nx];

    // Now we do the main calculation. This is a nested loop,
    // iterating over time (j) first, then over x values (i).
    for (j = 1; j <= Nt; ++j) {
        // Compute and fill in next array of values for this time step.
        // The new values are saved into the next_values array.
        for (i = Nx-1; i > 0; --i) {
            diff = curr_values[i+1] - 2 * curr_values[i] + curr_values[i-1];
            next_values[i] = curr_values[i] + r * diff;
        }
        // Next we swap curr_values & next_values for the next iteration
        temp = curr_values;
        curr_values = next_values;
        next_values = temp;
    }

    // Output final results
    puts("T(.,t) values at final time step:");
    print_values(curr_values);

    // Clean up and exit
    free(curr_values);
    free(next_values);
    return EXIT_SUCCESS;
}

/* This function defines our initial condition.
 * We'll use a simple step function for this.
 */
double initial_condition(double x) {
    return (x >= 0.4 && x <= 0.6) ? 1 : 0.2;
}

/* Reads in the user-specified command line arguments
 * and sets the appropriate globals as appropriate.
 *
 * Returns non-zero if all arguments have been specified
 * correctly, otherwise zero.
 */
int read_parameters(int argc, char *argv[]) {
    if (argc != 3) {
        fputs("Required arguments: <x_partitions(Nx)> <t_end>\n", stderr);
        return 0;
    }
    Nx = atol(argv[1]);
    if (Nx < 1) {
        fputs("Number of x partitions must be >= 1\n", stderr);
        return 0;
    }
    t_end = atof(argv[2]);
    if (t_end <= 0.0) {
        fputs("Time endpoint must be positive\n", stderr);
        return 0;
    }
    return 1;
}

/* Prints the given array of values for a particular time step */
void print_values(double *values) {
    long i;

    printf("[");
    for (i = 0; i <= Nx; ++i) {
        printf("%.10f", values[i]);
        if (i < Nx) printf(", ");
    }
    printf("]\n");
}
