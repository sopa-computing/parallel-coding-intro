#include <stdio.h>
#include <stdlib.h>

/**
 * Hello World, C style!
 */
int main(void) {
    puts("Hello World!");
    return EXIT_SUCCESS;
}

/* Notes for C newbies:
 *
 * - The two #include lines tell the C compiler that you might be using some
 *   functions and definitions provided by code libraries. <stdio.h> provides
 *   functionality for input & output - we're using its puts() function here.
 *   (This is a wee bit like 'from ... import *' in Python.)
 *
 * - C uses curly braces {...} to delimit the start and end of code blocks.
 *   We normally use indentation as well to make it readable.
 *   (Python uses indentation only!)
 *
 * - "int main(void) { ... }" defines a function called main. It returns an
 *   integer (int) and doesn't take any arguments (void). The function body
 *   defines what it will do. In this case, it uses the 'puts' function from
 *   the C standard library to print out the string 'Hello World' (with a newline),
 *   then it exits and returns the number 0, which is here specified more verbosely
 *   by the constant EXIT_SUCCESS defined in <stdlib.h>. (This is a matter of style,
 *   but I like it as it makes things a bit more obvious.)
 *
 * - main() is a special function in C: whenever you run a C program it will invoke
 *   the main function.
 *
 * - So this program basically prints out "Hello World", then returns 0 back to the
 *   operating system. Returning 0 indicates that your program has succeeded;
 *   you should return non-zero if you want to indicate some kind of error.
 *
 * - The main() function can either be defined to take no arguments (as we have here)
 *   or arguments that allow you to access options passed on the command line. We'll
 *   see this form of main in the other examples.
 */
