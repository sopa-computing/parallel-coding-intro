# Serial Fortran codes

This directory contains the initial serial versions of our example codes,
written in Fortan 95.

**Disclaimer:** I'm not very familiar with Fortran. This code works but
may not follow best practice! A Fortran guru would be most welcome to come
and improve this code!

If you prefer C (as I do!), then C versions of this code can be found in the
adjacent **serial-c** folder.

## How to compile the code

The code is written in Fortran so first needs to be **compiled** into runnable
binary code. I have provided a **Makefile** to do this for you. (A Makefile is
essentially an old-fashioned and hard-to-read recipe for building code projects.)
You can invoke this by typing:

```sh
make
```

Note that you need to run this from the directory containing this file.

This Makefile should work on any Linux computer with the GCC compiler suite,
and it includes some alternative instructions for compiling the code using the
Intel compiler suite, which is available on School Linux computers and on Eddie.
If you're familiar with building Fortran code, it should be possible to tweak the
Makefile to make the code work with other compilers or on MacOS or Windows
computers.

## How to run the code

Once compiled, you can run the codes by typing in:

```sh
./hello_world

OR

./monte_carlo_pi 1000000

OR

./heat_equation 100 10
```

Note that the **monte_carlo_pi** and **heat_equation** codes require you to
supply one or two parameters. You'll get told what information you need
if you simply try to run them without parameters.
