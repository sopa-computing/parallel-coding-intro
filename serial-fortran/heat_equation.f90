! Heat equation, serial Fortran version.
!
! Please see the C version for background information
! and an explanation of the variables.
program heat_equation
    implicit none
    logical :: valid ! Tests whether user-specified parameters are all valid
    integer :: Nx ! Number of x sub-intervals to pick from interval [0,1]
    double precision :: t_end ! Final time to reach
    double precision, parameter :: alpha = 0.001d0 ! alpha coefficient in heat equation
    double precision, parameter :: r = 0.25d0 ! Must be <1/2 for convergence
    double precision :: dt ! Time increment (computed from params)
    integer :: Nt ! Number of time iterations required (computed from params)
    integer :: i, j ! i = discrete 'x' point (0..Nx), j = discrete 't' point (0..Nt)
    double precision, dimension(:), allocatable, target :: work1, work2 ! Memory arrays used for curr_values & next_values (below)
    double precision, dimension(:), pointer :: curr_values, next_values ! Pointers to memory arrays for storing current & next time step values
    double precision, dimension(:), pointer :: temp ! Used for swapping buffers
    double precision :: diff ! Temporary variable used during inner calculation loop
    double precision :: initial_condition ! Function to compute initial conditions (see below)
    interface
        subroutine print_values(values)
            double precision, dimension(:), pointer :: values
        end subroutine
    end interface

    ! Read in and validate user-specified parameters
    call read_parameters(Nx, t_end, valid)
    if (.not. valid) then
        stop 1
    end if

    ! Calculate computed parameters
    ! We first pick dt so that the resulting r will ensure convergence,
    ! then calculate how many time steps we need (Nt) to reach required end time.
    dt = r / (Nx * Nx * alpha)
    Nt = ceiling(t_end / dt)

    write(*,"(A,I0,A,F10.4,A,F10.8,A,F10.8)") "Input parameters: Nx=", Nx, &
        " t_end=", t_end, " alpha=", alpha, " r=", r
    write(*,"(A,F10.4,A,I0)") "Computed parameters: dt=", dt, " Nt=", Nt

    ! Allocate memory arrays to hold current & next time step iteration data
    allocate(work1(Nx + 1))
    allocate(work2(Nx + 1))

    ! We will first use curr_values to store the current (initial, j=0)
    ! time step data, and next_values for the next (j=1) data.
    curr_values => work1
    next_values => work2

    ! Set up initial conditions
    ! We will first use curr_values to store the current (initial, j=0) time step data,
    ! and next_values for the next (j=1) data.
    do i = 1, Nx + 1
        curr_values(i) = initial_condition(dble(i-1) / Nx)
    end do

    ! Fix boundary conditions for the first (and subsequent) iterations
    next_values(1) = curr_values(1)
    next_values(Nx + 1) = curr_values(Nx + 1)

    ! Now we do the main calculation. This is a nested loop,
    ! iterating over time (j) first, then over x values (i).
    do j = 1, Nt
        ! Compute and fill in next array of values for this time step.
        ! The new values are saved into the next_values array.
        do i = 2, Nx
            diff = curr_values(i+1) - 2 * curr_values(i) + curr_values(i-1)
            next_values(i) = curr_values(i) + r * diff
        end do
        ! Next we swap curr_values & next_values for the next iteration
        temp => curr_values
        curr_values => next_values
        next_values => temp
    end do

    ! Output final results
    write(*,"(A)") "T(.,t) values at final time step:"
    call print_values(curr_values)

end program heat_equation


! This function defines our initial condition.
! We'll use a simple step function for this.
double precision function initial_condition (x) result (T)
    implicit none
    double precision :: x

    if ((x >= 0.4d0) .and. (x <= 0.6d0)) then
        T = 1.0d0
    else
        T = 0.2d0
    end if

end function initial_condition


! Reads in the user-specified command line arguments
! and sets the appropriate globals as appropriate.
!
! Sets valid to .TRUE. if all arguments have been specified
! correctly, otherwise .FALSE.
subroutine read_parameters(Nx, t_end, valid)
    implicit none
    integer, intent(out) :: Nx
    double precision, intent(out) :: t_end
    logical, intent(out) :: valid
    character(len=12) :: arg

    valid = .FALSE.
    if (command_argument_count() /= 2) then
        write(0,"(A)") "Required arguments: <x_partitions(Nx)> <t_end>"
        return
    end if
    call get_command_argument(1, arg)
    read(arg,*) Nx
    if (Nx < 1) then
        write(0,"(A)") "Number of x partitions must be >= 1"
        return
    end if
    call get_command_argument(2, arg)
    read(arg,*) t_end
    if (t_end <= 0.0d0) then
        write(0,"(A)") "Time endpoint must be positive"
        return
    end if
    valid = .TRUE.

end subroutine read_parameters


! Prints the given array of values for a particular time step
subroutine print_values(values)
    implicit none
    double precision, dimension(:), pointer :: values
    integer :: i

    write(*,"(A)", advance="no") "["
    do i = 1, size(values)
        write(*,"(F12.10)", advance="no") values(i)
        if (i < size(values)) then
            write(*,"(A)", advance="no") ", "
        end if
    end do
    write(*,"(A)") "]"

end subroutine print_values
