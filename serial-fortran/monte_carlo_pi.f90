! Estimation of Pi via Monte Carlo simulation.
! Serial Fortran version
program monte_carlo_pi
    implicit none
    character(len=20) :: arg
    integer :: num_trials ! Number of Monte Carlo trials to perform
    integer :: success_count ! Number of MC successes recorded
    double precision :: pi_estimate; ! Final Pi estimate

    ! Read requested number of trials from command line arguments
    if (command_argument_count() /= 1) then
        write(0,"(A)") "Required argument: <number of trials>"
        stop 1
    end if
    call get_command_argument(1, arg)
    read(arg,*) num_trials
    if (num_trials <= 0) then
        write(0,"(A)") "Number of trials must be a positive integer"
        stop 1
    end if

    ! Run specified number of Monte Carlo trials
    call do_monte_carlo(num_trials, success_count)

    ! Compute our resulting estimate for pi
    pi_estimate = 4.0d0 * success_count / num_trials

    ! Output final result
    write(*,"(A,F12.10,A,I0,A,I0)") "pi=~", pi_estimate, &
        " num_trials=", num_trials, &
        " success_count=", success_count

end program monte_carlo_pi


! Runs the Monte Carlo simulation for the given number
! of trials, returning the number of successful samples.
subroutine do_monte_carlo(num_trials, success_count)
    implicit none
    integer, intent(in) :: num_trials
    integer, intent(out) :: success_count
    integer :: i
    real :: x,y

    success_count = 0
    do i = 1, num_trials
        call random_number(x)
        call random_number(y)
        if ((x - 0.5)**2 + (y - 0.5)**2 < (0.5)**2) then
            success_count = success_count + 1
        end if
    end do

end subroutine do_monte_carlo
